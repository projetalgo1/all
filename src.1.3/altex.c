/** Programme TP Ensimag 2A - AOD. Justification optimale d'un texte.
 * @author Jean-Louis.Roch@imag.fr, David.Glesser@imag.fr
 * @date 15/9/2014
 */

#include "altex-utilitaire.h"
#include <stdio.h>
#include <sys/stat.h>
#include <math.h>
#include <stdlib.h>
#include <locale.h>
#include <limits.h>
#include <unistd.h>


/** Optimal alignment of a text (read from input file in) with ligns of fixed length (M units) written on output file (out).
  * @param in: input stream, in ascii (eg in == stdin)
  * @param len: maximum number of words in the text (may be larger than the stream)
  * @param outformat: output stream, should be initialed
  * @param M: line size (in units)
  * @param N: function to optimize is #spaces^N 
  * @return: the cost of an optimal alignment
  */
long altex(FILE* in, size_t len, struct stream *outformat, unsigned long M, unsigned N) ;

  
long altex(FILE* in, size_t len, struct stream *outformat, unsigned long M, unsigned N) {
   char* buffer = (char*) calloc(len+1, sizeof(char) ) ;  // Preallocated buffer, large enough to store any word...
   size_t buffer_size = len ; 
   struct parser p_in; 
   init_parser(in, &p_in) ; 
   long maxval_all_paragraphs = 0 ;
   long sumval_all_paragraphs = 0 ;
   int endofile = 0;
   while (!endofile) { // We read each paragraph from the first one
      int is_paragraph_end = 0 ;
        while ( !is_paragraph_end ) { // All words in the paragraph are stored in tabwords, character being stored in buffer..
         size_t n = read_word( &p_in, &is_paragraph_end, buffer, buffer_size ) ; // Size of the word if any
         if  (n==0) { endofile = 1 ; break ; }
         else { // word of length n 
            printf("%s ", buffer) ;
         }
       }
       printf("\n\n ") ;
   }
   
   
   free(buffer) ;
   fprintf(stderr, "\n#@alignment_cost:MAX=%ld;SUM=%ld\n", maxval_all_paragraphs, sumval_all_paragraphs);
   
   return maxval_all_paragraphs;
}


//////////////////////////////////////////////////////////////////////////////


static void usage(char * prog) {
   fprintf(stderr, "usage: %s -i input_file -o output_file -f format [-m M]\n", prog);
   fprintf(stderr, "\nOptions:\n");
   fprintf(stderr, "\t-i input_file   Input file (default: stdin)\n");
   fprintf(stderr, "\t-o output_file  Output file (default: stdout)\n");
   fprintf(stderr, "\t-f format       Output format, possible formats: \n");
   fprintf(stderr, "\t                    'text' for a text file (default)\n");
   fprintf(stderr, "\t                    'serif' for a pdf file with a serif font\n");
   fprintf(stderr, "\t                    'hand' for a pdf file with a handwritten font\n");
   fprintf(stderr, "\t-m M            Where M is the width (default: 80)\n");
   fprintf(stderr, "\t                    if the format is 'text', it's the number of characters\n");
   fprintf(stderr, "\t                    else, it's in points and should be < %i points\n", (int)A4_WIDTH);
   exit(1);
}


int main(int argc, char *argv[] ) {
   //Command line parsing
   char c;
   
   long M = 80 ;
   char* input_file = 0;
   char* output_file = 0;
   char* format = 0;

   while ((c = getopt(argc , argv, "i:o:f:m:")) != -1)
   {
      switch (c) {
         case 'i':
            input_file = optarg;
            break;
         case 'o':
            output_file = optarg;
            break;
         case 'f':
            format = optarg;
            break;
         case 'm':
            M = atoi(optarg);
            break;
         case '?':
            usage(argv[0]);
            break;
      }
   }


   FILE* f = stdin;
   if(input_file)
   {
      f = fopen(input_file, "r") ; 
      if( f == NULL ) {
         fprintf( stderr, "Error opening input file.");
         return(-1);
      }
   }
   
   struct stat st;
   fstat(fileno(f), &st);
   size_t fsize = st.st_size;
  

   if (!setlocale(LC_CTYPE, "")) {
       fprintf( stderr, "Can't set the specified locale! "
                        "Check LANG, LC_CTYPE, LC_ALL.\n");
       return -1;
   }
   
   
   struct stream* outstream;
   
   if(format == 0)
      outstream = init_stream(output_file, 0, M);
   else {
      switch(format[0])
      {
         case 't': //"text"
            outstream = init_stream(output_file, 0, M);
            break;
         case 's': //"serif"
            outstream = init_stream(output_file, "DejaVuSerif.ttf", M);
            break;
         case 'h': //"hand"
            outstream = init_stream(output_file, "daniel.ttf", M);
            break;
         default:
            fprintf( stderr, "Unrecognized format.\n");
            return 1;
      }
   }
   
   if(!outstream)
      return 1;

   altex(f, fsize, outstream, M,  2) ; 
   
   //free and flush outstream
   free_format(outstream);
   fclose( f ) ; 
   return 0 ;
}


 
